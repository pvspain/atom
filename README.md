**atom.io** editor configuration files.

1) Open a terminal in the root folder of your atom config

* Linux `cd ${HOME}/.atom`

2) Clone this repo into the root folder of your atom config:

* Linux `git clone https://pvspain@bitbucket.org/pvspain/atom.git`

3) This will create an `atom` folder inside your root folder.
  Move the content of the `atom` folder to the root folder and delete the `atom` folder.

* Linux & bash
```
# include .git in glob expansion below
# run in a separate process to localise effect of shopt command
# -b option for mv creates backups for any files in root folder that would otherwise be clobbered
( shopt -s dotglob; mv -b atom/* . && rm -r atom )
```